export $(cat .env | grep -v ^# | xargs)

docker build -t $DOCKER_IMAGE_NAME . || exit

docker stop $DOCKER_CONTAINER_NAME || echo "Nothing to stop"
docker rm $DOCKER_CONTAINER_NAME || echo "Nothing to rm"

docker run \
  -d \
  --hostname $RABBIT_HOSTNAME \
  --name $DOCKER_CONTAINER_NAME \
  -p 5672:5672 \
  -p 15672:15672 \
  $DOCKER_IMAGE_NAME

