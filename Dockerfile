FROM rabbitmq:3.7.2-management

# Rabbitmq
EXPOSE 5672
# Management plugin
EXPOSE 15672
